package ua.com.java4beginners.servlets;

import ua.com.java4beginners.dao.Crud;
import ua.com.java4beginners.models.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(loadOnStartup = 1, urlPatterns = "/")
public class ProductListServlet extends HttpServlet {

    List<Product> productList;
    Crud model;
    Product product;

    static Map<Long, Product> products = new HashMap<Long, Product>() {{
        put(1L, new Product(1L, "NoteBook", "Just a notebook.", 100, null, 20));
        put(2L, new Product(2L, "Cup", "Just a cup.", 125, null, 10));
    }};

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        PrintWriter out = resp.getWriter();
//        out.write("<html><body><h1>Products</h1><ul>");
//        for (Product product : products.values()) {
//            out.write(String.format("<li>%s, %d", product.getTitle(), product.getPrice()));
//            String addToCartButtonHtml = "<form method='POST' action='/cart/products'>" +
//                    "<input type='hidden' name='id' value='%d'>" +
//                    "<button type='submit'>Add</button>" +
//                    "</form>";
//            out.write(String.format(addToCartButtonHtml, product.getId()));
//        }
//        out.write("</ul></body></html>");

        //My method
//        PrintWriter out = resp.getWriter();
//        out.write("<html><body><h1>Products</h1><ul>");
//
//        try {
//            productList = model.getAllProducts();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        for (Product product : productList) {
//            out.write(String.format("<li>%s, %d, $d", product.getTitle(), product.getPrice(), product.getAmount()));
//            String addToCartButtonHtml = "<form method='POST' action='/cart/products'>" +
//                    "<input type='hidden' name='id' value='%d'>" +
//                    "<button type='submit'>"+ product.getTitle() + "</button>" +
//                    "</form>";
//            out.write(String.format(addToCartButtonHtml, product.getId()));
//        }
//        out.write("</ul></body></html>");
//
        PrintWriter out = resp.getWriter();
        out.write("<html><body><h1>Products</h1><ul>");

        try {
            product = model.getProduct();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        out.write(String.format("<li>%s, %d, $d", product.getTitle(), product.getPrice(), product.getAmount()));
        String addToCartButtonHtml = "<form method='POST' action='/cart/products'>" +
                "<input type='hidden' name='id' value='%d'>" +
                "<button type='submit'>" + product.getTitle() + "</button>" +
                "</form>";
        out.write(String.format(addToCartButtonHtml, product.getId()));

        out.write("</ul></body></html>");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String addToCartButtonHtml = "<form method='POST' action='/cart/products'>" +
                "<input type='hidden' name='id' value='%d'>" +
                "<button type='submit'>Add</button>" +
                "</form>";

    }
}
