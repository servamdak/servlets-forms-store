package ua.com.java4beginners.dao;

import ua.com.java4beginners.models.Product;

import java.sql.SQLException;
import java.util.List;

public interface Crud {
    void save(long clientId, long productId) throws SQLException, ClassNotFoundException;
    void delete(long productId) throws SQLException, ClassNotFoundException;
    void createOrder(long productId) throws SQLException, ClassNotFoundException;
    List<Product> getAllProducts() throws SQLException, ClassNotFoundException;
    Product getProduct() throws ClassNotFoundException, SQLException;
}
