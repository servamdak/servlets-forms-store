package ua.com.java4beginners.dao;

import ua.com.java4beginners.models.Product;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class CrudImplementation implements Crud {

    public String jdbcDriver = "org.postgresql.Driver";
    public String url = "jdbc:postgresql://localhost:5432/store";
    public String user = "postgres";
    public String password = "12321";

    public Connection connection;
    public PreparedStatement preparedStatement;
    public ResultSet resultSet;

    public String sqlQuery;

    public List<Product> productsList;
    public Product product;


    public void save(long clientId, long productId) throws SQLException, ClassNotFoundException {
        try {
            Class.forName(jdbcDriver);
            connection = DriverManager.getConnection(url, user, password);

            sqlQuery = "INSERT INTO Orders VALUES(?, ?)";
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, clientId);
            preparedStatement.setLong(2, productId);
            preparedStatement.execute();

            connection.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void delete(long productId) throws SQLException, ClassNotFoundException {
        try {
            Class.forName(jdbcDriver);
            connection = DriverManager.getConnection(url, user, password);

            sqlQuery = "DELETE FROM Orders WHERE PRODUCT_ID=?";
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, productId);
            preparedStatement.executeUpdate();

            connection.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createOrder(long productId) throws SQLException, ClassNotFoundException {
        try {
            Class.forName(jdbcDriver);
            connection = DriverManager.getConnection(url, user, password);
            connection.setAutoCommit(false);

            sqlQuery = "DELETE FROM Orders";
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.execute();

            sqlQuery = "INSERT INTO Orders VALUES(?)";
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, productId);
            preparedStatement.executeUpdate();

            connection.close();
            connection.setAutoCommit(true);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Product> getAllProducts() throws SQLException, ClassNotFoundException{
        try {
            Class.forName(jdbcDriver);
            connection = DriverManager.getConnection(url, user, password);
            productsList = new LinkedList<>();

            sqlQuery = "SELECT * FROM Products";
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                productsList.add(new Product(resultSet.getLong("ID"),
                        resultSet.getString("TITLE"),
                        resultSet.getString("DESCRIPTION"),
                        resultSet.getInt("PRICE"),
                        resultSet.getString("IMAGEURL"),
                        resultSet.getInt("AMOUNT")));
            }

            return productsList;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productsList;
    }

    public Product getProduct() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/store", "postgres", "12321");
        sqlQuery = "SELECT * FROM Products WHERE ID=?";

        preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setLong(1, 1);
        resultSet = preparedStatement.executeQuery();
        product = new Product(resultSet.getLong("ID"),
                resultSet.getString("TITLE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getInt("PRICE"),
                resultSet.getString("IMAGEURL"),
                resultSet.getInt("AMOUNT"));

        return product;
    }

//        sqlQuery = "SELECT TITLE, DESCRIPTION, PRICE, IMAGEURL FROM Products WHERE ID=?";
//        preparedStatement = connection.prepareStatement(sqlQuery);
//        preparedStatement.setLong(1, orderId);
//        resultSet = preparedStatement.executeQuery();
//
//        while (resultSet.next()) {
//            product = new Product(orderId, resultSet.getString("TITLE"),
//                    resultSet.getString("DESCRIPTION"),
//                    resultSet.getInt("PRICE"),
//                    resultSet.getString("IMAGEURL"));
//        }

}
