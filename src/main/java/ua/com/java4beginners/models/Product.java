package ua.com.java4beginners.models;

public class Product {
    private Long id;
    private String title;
    private String description;
    private Integer price;
    private String imageUrl;
    private Integer amount;

    public Product(Long id, String title, String description, Integer price, String imageUrl, Integer amount) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Integer getAmount() {
        return amount;
    }
}
