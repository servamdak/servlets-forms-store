package ua.com.java4beginners.models;

import ua.com.java4beginners.models.Product;

import java.util.List;

public class Order {
    private Long orderId;
    private List<Product> products;

    public Order(Long orderId, List<Product> products) {
        this.orderId = orderId;
        this.products = products;
    }

    public Long getOrderId() {
        return orderId;
    }

    public List<Product> getProducts() {
        return products;
    }
}
